#!/bin/bash
channel=$2

token=$(pass show slack-work-webhook-token)
url="https://hooks.slack.com/services/$token"
payload=""

if [ -z $channel ]
then
  payload='payload={"text": "'$1'", "unfurl_links": true, "icon_emoji": ":doge:"}'
else
  payload='payload={"channel": "'$channel'", "text": "'$1'", "unfurl_links": true, "icon_emoji": ":doge:"}'
fi

curl -X POST --data-urlencode $payload $url
